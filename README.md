# La pagaie du gat

Mon premier site web, pour l'association de connoë & kayak de mes parents, en 2004.

*Le 16/10/2007 l'association à été renomée **Explorateurs de Gascogne** pour élargir son champs d'activité.*

Site optimisé pour un chargement rapide avec une connexion internet par modem 56k.

Url d'époque : http://lapagaiedugat.free.fr/

Url avec les derniers changements : https://explorateurs-de-gascogne.frama.io/
